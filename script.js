const getCube = 2 ** 3;
console.log(`The cube of 2 is ${getCube}`);

const address = [258, 'Washington Ave NW', 'California', 90011];
const [stNumber, stName, state, zipCode] = address;

console.log(`I live at ${stNumber} ${stName}, ${state} ${zipCode}`);

const animal = {
	name: 'Lolong',
	type: 'salwater crocodile',
	weight: '1075 kgs',
	length: '20 ft 3 in'
}

const {name, type, weight, length} = animal;

console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${length}.`);

const array = [1, 2, 3, 4, 5,];

array.forEach((number) => console.log(number));

const reduceNumber = array.reduce((x, y) => x + y);

console.log(reduceNumber);

class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const newDog = new Dog('Frankie', 5, 'Miniature Dachshund');
console.log(newDog);